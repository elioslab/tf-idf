package wondertech.tf.idf;

import java.util.Arrays;
import java.util.List;

public class TFIDFCalculator {

    public double tf(List<String> doc, String term) {
        double result = 0;
        for (String word : doc) {
            if (term.equalsIgnoreCase(word))
                result++;
        }
        return result / doc.size();
    }

    public double idf(List<List<String>> docs, String term) {
        double n = 0;
        for (List<String> doc : docs) {
            for (String word : doc) {
                if (term.equalsIgnoreCase(word)) {
                    n++;
                    break;
                }
            }
        }
        return Math.log(docs.size() / n);
    }

    public double tfIdf(List<String> doc, List<List<String>> docs, String term) {
        return tf(doc, term) * idf(docs, term);
    }

    public static void main(String[] args) {
        List<String> doc1 = Arrays.asList("Lorem", "ipsum", "dolor", "ipsum", "sit", "ipsum");
        List<String> doc2 = Arrays.asList("Vituperata", "incorrupte", "at", "ipsum", "pro", "quo");
        List<String> doc3 = Arrays.asList("Has", "persius", "disputationi", "id", "simul");

        List<List<String>> documents = Arrays.asList(doc1, doc2, doc3);

        TFIDFCalculator calculator = new TFIDFCalculator();
        double ipsum_doc1 = calculator.tfIdf(doc1, documents, "ipsum");
        double vituperata_doc1 = calculator.tfIdf(doc1, documents, "vituperata");
        double ipsum_doc2 = calculator.tfIdf(doc2, documents, "ipsum");
        double vituperata_doc2 = calculator.tfIdf(doc2, documents, "vituperata");

        System.out.println("Doc1 TF-IDF (ipsum) = " + ipsum_doc1);
        System.out.println("Doc1 TF-IDF (Vituperata) = " + vituperata_doc1);
        System.out.println("Doc2 TF-IDF (ipsum) = " + ipsum_doc2);
        System.out.println("Doc2 TF-IDF (Vituperata) = " + vituperata_doc2);
    }
}